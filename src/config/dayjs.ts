import dayjs from "dayjs";
import 'dayjs/locale/ru';
import {MONTH_LIST_FULL, MONTH_LIST_SHORT, DAYJS_WEEK_DAYS_FULL, DAYJS_WEEK_DAYS_MIN, DAYJS_WEEK_DAYS_SHORT} from "../constants/dates";

const locale = {
    name: 'ru',
    weekStart: 1,
    yearStart: 4,
    monthsShort: MONTH_LIST_SHORT,
    months: MONTH_LIST_FULL,
    weekdaysMin: DAYJS_WEEK_DAYS_MIN,
    weekdaysShort: DAYJS_WEEK_DAYS_SHORT,
    weekdays: DAYJS_WEEK_DAYS_FULL,
};

dayjs.locale('ru', locale);
