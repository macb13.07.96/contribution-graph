import {Dayjs} from "dayjs";

export interface IGraphDayItem {
    key: string;
    date: Dayjs;
    countContributions: number;
}

export type TGraphArray = IGraphDayItem[];

export interface IGraphCalendar {
    days: TGraphArray,
    months: string[],
}
