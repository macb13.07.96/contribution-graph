export interface IApiResponse {
    [key: string]: number;
}
