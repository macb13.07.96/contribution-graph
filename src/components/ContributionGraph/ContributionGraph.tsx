import axios, {AxiosResponse} from "axios";
import React, {useEffect, useMemo, useState} from "react";
import {
    StyledContainer,
    StyledContributionGraphContainer,
    StyledGraphBodyContainer,
    StyledGraphDaysContainer,
    StyledGraphMonth,
    StyledGraphTitleContainer,
    StyledGraphWeekDay,
    StyledGraphWeekDaysContainer,
    StylesSuggestionPosition
} from "./styled";
import {API_URL} from "../../constants/api";
import {buildGraphCalendar} from "../../utils/buildGraphCalendar";
import {WEEK_DAYS_MIN} from "../../constants/dates";
import {GraphDay} from "./components";
import {GraphSuggestions} from "./components/GraphSuggestions";
import {IApiResponse} from "../../types/api";

const WEEK_DAYS_RENDER: Record<string, boolean> = {
    0: true,
    2: true,
    4: true,
};

export const ContributionGraph: React.FC = () => {
    const [contributions, setContributions] = useState<IApiResponse>({});
    const [isError, setIsError] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(false);

    useEffect(() => {
        setIsLoading(true);

        axios.get<void, AxiosResponse<IApiResponse>>(API_URL)
            .then<IApiResponse>((response) => {
                setContributions(response.data);
                return response.data;
            })
            .catch((error) => {
                setIsError(true);
                console.error(error);
            })
            .finally(() => setIsLoading(false));
    }, []);

    const {days, months} = useMemo(() => buildGraphCalendar(contributions), [contributions]);

    if (isLoading) return <>Загрузка...</>;

    if (isError) return <>Ошибка загрузки данных с сервера!</>;

    return (
        <StyledContainer>
            <StyledContributionGraphContainer>
                <StyledGraphTitleContainer>
                    {months.map((value) => (
                        <StyledGraphMonth key={value}>
                            {value}
                        </StyledGraphMonth>
                    ))}
                </StyledGraphTitleContainer>
                <StyledGraphBodyContainer>
                    <StyledGraphWeekDaysContainer>
                        {WEEK_DAYS_MIN.map((value, index) => (
                            <StyledGraphWeekDay key={value}>
                                {WEEK_DAYS_RENDER[`${index}`] && value}
                            </StyledGraphWeekDay>
                        ))}
                    </StyledGraphWeekDaysContainer>
                    <StyledGraphDaysContainer>
                        {days.map(({key, date, countContributions}) => (
                            <GraphDay key={key} date={date} countContributions={countContributions}/>
                        ))}
                    </StyledGraphDaysContainer>
                </StyledGraphBodyContainer>
                <StylesSuggestionPosition>
                    <GraphSuggestions/>
                </StylesSuggestionPosition>
            </StyledContributionGraphContainer>
        </StyledContainer>
    )
}
