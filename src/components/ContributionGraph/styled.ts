import styled from "styled-components";
import {getDayColor} from "../../utils/getDayColor";

export const StyledContainer = styled.div`
  & {
    width: 1120px;
    padding: 55px 95px;
    justify-content: center;
  }
`

export const StyledContributionGraphContainer = styled.div`
  & {
    display: flex;
    width: 100%;
    flex-direction: column;
    align-items: start;
    justify-content: start;
  }
`

export const StyledGraphTitleContainer = styled.div`
  & {
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: calc(52 * 2px + 52 * 15px);
    padding: 0 34px;
    margin-bottom: 5px;
  }
`;

export const StyledGraphMonth = styled.span`
  & {
    color: #959494;
    font-family: Inter, sans-serif;
    font-weight: 400;
    font-size: 12px;
    line-height: 1.2;
    text-align: center;
    vertical-align: middle;
  }
`

export const StyledGraphWeekDaysContainer = styled.div`
  & {
    display: flex;
    height: 100%;
    flex-direction: column;
    width: 15px;
    row-gap: 2px;
  }
`;

export const StyledGraphWeekDay = styled.span`
  & {
    width: 15px;
    height: 15px;
    color: #959494;
    font-size: 12px;
    line-height: 1.2;
    text-align: center;
    vertical-align: middle;
    font-weight: 400;
    font-family: Inter, sans-serif;
  }
`

export const StyledGraphBodyContainer = styled.div`
  & {
    display: flex;
    column-gap: 4px;
    width: 100%;
  }
`

export const StyledGraphDaysContainer = styled.div`
  & {
    height: calc(7 * 15px + 6 * 2px);
    width: calc(50 * 2px + 51 * 15px);
    display: flex;
    gap: 2px;
    flex-direction: column;
    flex-wrap: wrap;
  }
`;

export const StyledGraphDay = styled.div<{ count: number }>`
  & {
    position: relative;
    width: 15px;
    height: 15px;
    background-color: ${({count}) => getDayColor(count)};
  }

  &:hover {
    border: 1px solid rgba(0, 0, 0, .5);
  }
`

export const StylesSuggestionPosition = styled.div`
  & {
    margin-top: 15px;
    margin-left: 19px;
  }
`
