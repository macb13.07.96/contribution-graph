import React, {useState} from "react";
import {Dayjs} from "dayjs";
import {StyledGraphDay} from "../../styled";
import {Tooltip} from "../../../Tooltip";
import {getTooltipDescription, getTooltipTitle} from "../../../../utils/tooltipUtils";

interface IProps {
    countContributions: number;
    date?: Dayjs;
    squashContributionsCount?: boolean;
}

export const GraphDay: React.FC<IProps> = ({date, countContributions, squashContributionsCount = false}) => {
    const [isTooltipOpen, setIsTooltipOpen] = useState<boolean>(false);

    const handleToggle = () => setIsTooltipOpen(prev => !prev);
    const handleClose = () => setIsTooltipOpen(false);

    return (
        <StyledGraphDay count={countContributions} onClick={handleToggle}>
            {isTooltipOpen && (
                <Tooltip
                    title={getTooltipTitle(countContributions, squashContributionsCount)}
                    description={date ? getTooltipDescription(date) : undefined}
                    onClose={handleClose}
                />
            )}
        </StyledGraphDay>
    )
}
