import React from "react";
import {StyledGraphSuggestionContainer, StyledSuggestionsList, StyledSuggestionText} from "./styled";
import {GraphDay} from "../GraphDay";

const suggestionArray = [
    0, 1, 10, 20, 30
]

export const GraphSuggestions: React.FC = () => {

    return (
        <StyledGraphSuggestionContainer>
            <StyledSuggestionText>
                Меньше
            </StyledSuggestionText>
            <StyledSuggestionsList>
                {suggestionArray.map((count) => (
                    <GraphDay key={count} countContributions={count} squashContributionsCount={true}/>
                ))}
            </StyledSuggestionsList>
            <StyledSuggestionText>
                Больше
            </StyledSuggestionText>
        </StyledGraphSuggestionContainer>
    )
}
