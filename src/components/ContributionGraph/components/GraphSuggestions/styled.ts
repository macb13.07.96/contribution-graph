import styled from "styled-components";

export const StyledGraphSuggestionContainer = styled.div`
  & {
    display: flex;
    column-gap: 7px;
    align-items: center;
  }
`;

export const StyledSuggestionsList = styled.div`
  & {
    display: flex;
    column-gap: 2px;
    align-items: center;
  }
`;

export const StyledSuggestionText = styled.span`
  & {
    font-size: 8px;
    font-weight: 400;
    font-family: Inter, sans-serif;
    line-height: 1.2;
    color: #959494;
  }
`
