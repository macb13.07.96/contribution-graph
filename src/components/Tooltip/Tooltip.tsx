import React, {useEffect, useRef} from "react";
import {StyledTooltipBody, StyledTooltipContainer, StyledTooltipDescription, StyledTooltipTitle} from "./styled";

interface IProps {
    title: string;
    description?: string;
    onClose?: () => void;
}

export const Tooltip: React.FC<IProps> = ({description, title, onClose}) => {
    const ref = useRef<HTMLDivElement>(null)

    useEffect(() => {
        const handleOutsideClick = (event: MouseEvent) => {
            const target = event.target as HTMLElement;

            if (!ref.current?.parentNode?.contains(target)) {
                onClose?.();
            }
        }

        document.addEventListener('click', handleOutsideClick);

        return () => {
            document.removeEventListener('click', handleOutsideClick);
        }
    }, [])

    return (
        <StyledTooltipContainer ref={ref}>
            <StyledTooltipBody>
                <StyledTooltipTitle>
                    {title}
                </StyledTooltipTitle>
                {description && (
                    <StyledTooltipDescription>
                        {description}
                    </StyledTooltipDescription>
                )}
            </StyledTooltipBody>
        </StyledTooltipContainer>
    )
}
