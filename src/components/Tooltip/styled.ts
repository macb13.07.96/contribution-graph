import styled from "styled-components";

export const StyledTooltipContainer = styled.div`
  & {
    width: fit-content;
    position: absolute;
    bottom: 7px;
    z-index: 100;
    left: 50%;
    transform: translateY(-15px) translateX(-50%);
  }
`

export const StyledTooltipBody = styled.div`
  & {
    width: fit-content;
    max-width: 300px;
    display: flex;
    flex-direction: column;
    row-gap: 4px;
    position: relative;
    background-color: rgb(0, 0, 0);
    padding: 5px 9px;
    border-radius: 3px;
  }

  &:after {
    position: absolute;
    display: block;
    background-color: rgb(0, 0, 0);
    content: '';
    width: 10px;
    height: 10px;
    left: 50%;
    transform: rotate(45deg) translateX(calc(-50% - 1px));
    z-index: -1;
    bottom: -8px;
  }
`;

export const StyledTooltipTitle = styled.h3`
  & {
    margin: 0;
    font-size: 12px;
    font-weight: 400;
    color: rgb(255, 255, 255);
    line-height: 1.2;
    font-family: Inter, sans-serif;
    white-space: nowrap;
    text-align: center;
  }
`;

export const StyledTooltipDescription = styled.span`
  & {
    display: block;
    color: #7C7C7C;
    font-size: 10px;
    font-family: Inter, sans-serif;
    font-weight: 400;
    line-height: 1.2;
    white-space: nowrap;
  }
`;
