import React from 'react';
import {ContributionGraph} from "./components/ContributionGraph";
import {createGlobalStyle} from "styled-components";
import './config/dayjs'

const GlobalStyles = createGlobalStyle`
  * {
    box-sizing: border-box;
  }
`

function App() {
    return (
        <>
            <GlobalStyles/>
            <ContributionGraph/>
        </>
    );
}

export default App;
