export const GRAPH_ENTRIES = {
    BASE: {
        min: 0,
        max: 0,
    },
    SMALL: {
        min: 1,
        max: 9,
    },
    MEDIUM: {
        min: 10,
        max: 19,
    },
    HIGH: {
        min: 20,
        max: 29,
    },
    VERY_HIGH: {
        min: 30,
        max: Infinity,
    }
}
