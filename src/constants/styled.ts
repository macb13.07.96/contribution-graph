export const GRAPH_DAY_COLORS = {
    BASE: "#EDEDED",
    SMALL: "#ACD5F2",
    MEDIUM: "#7FA8C9",
    HIGH: "#527BA0",
    VERY_HIGH: "#254E77"
}
