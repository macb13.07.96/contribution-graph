export const SERVER_DATE_FORMAT = 'YYYY-MM-DD';
export const TOOLTIP_DATE_FORMAT = 'dddd, MMMM DD, YYYY';

export const GRAPH_WEEK_COUNT = 51;
export const GRAPH_DAYS_IN_WEEK = 7;

export const MONTH_LIST_FULL = [
    'Январь',
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь"
]

export const MONTH_LIST_SHORT = [
    'Янв.',
    'Фев.',
    'Март',
    "Апр.",
    "Май",
    "Июнь",
    "Июль",
    "Авг.",
    "Сент.",
    "Окт.",
    "Нояб.",
    "Дек."
];

export const WEEK_DAYS_MIN = [
    'Пн',
    "Вт",
    "Ср",
    "Чт",
    "Пт",
    "Сб",
    "Вс"
]

export const DAYJS_WEEK_DAYS_SHORT = [
    "Воскр",
    'Пон',
    "Втор",
    "Среда",
    "Четв",
    "Пятн",
    "Суб",
]

export const DAYJS_WEEK_DAYS_FULL = [
    "Воскресенье",
    'Понедельник',
    "Вторник",
    "Среда",
    "Четверг",
    "Пятница",
    "Суббота",
]

export const DAYJS_WEEK_DAYS_MIN = [
    "Вс",
    'Пн',
    "Вт",
    "Ср",
    "Чт",
    "Пт",
    "Сб",
]
