import {IApiResponse} from "../types/api";
import {IGraphCalendar, IGraphDayItem, TGraphArray} from "../types/graph";
import dayjs, {Dayjs} from "dayjs";
import {GRAPH_DAYS_IN_WEEK, GRAPH_WEEK_COUNT, SERVER_DATE_FORMAT} from "../constants/dates";

const getDaysCountToEndWeek = (date: Dayjs): number => {
    const currentWeekDay = date.day();

    return 7 - currentWeekDay;
}

export const buildGraphCalendar = (graph: IApiResponse): IGraphCalendar => {
    const result: TGraphArray = [];

    const monthList = new Set<string>();

    const graphCellsCount  = GRAPH_DAYS_IN_WEEK * GRAPH_WEEK_COUNT;
    const graphDaysCount = graphCellsCount - getDaysCountToEndWeek(dayjs());

    for (let i = 0; i < graphDaysCount; i++) {
        const date = dayjs().subtract(i, 'day');

        monthList.add(date.format('MMM'));

        const formattedDate = date.format(SERVER_DATE_FORMAT);

        const item: IGraphDayItem = {
            key: formattedDate,
            countContributions: graph[formattedDate] ?? 0,
            date,
        };

        result.unshift(item);
    }

    return {
        days: result,
        months: Array.from(monthList).reverse(),
    };
}
