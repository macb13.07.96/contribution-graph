import {Dayjs} from "dayjs";
import {TOOLTIP_DATE_FORMAT} from "../constants/dates";
import {GRAPH_ENTRIES} from "../constants/graph";

export const getTooltipTitle = (count: number, squashCount: boolean = false): string => {
    const {SMALL, MEDIUM, HIGH, VERY_HIGH} = GRAPH_ENTRIES;
    switch (true) {
        case count >= SMALL.min && count <= SMALL.max: {
            return `${squashCount ? `${SMALL.min}-${SMALL.max}` : count} contributions`;
        }
        case count >= MEDIUM.min && count <= MEDIUM.max: {
            return `${squashCount ? `${MEDIUM.min}-${MEDIUM.max}` : count} contributions`;
        }
        case count >= HIGH.min && count <= HIGH.max: {
            return `${squashCount ? `${HIGH.min}-${HIGH.max}` : count} contributions`;
        }
        case count >= VERY_HIGH.min: {
            return `${squashCount ? `${VERY_HIGH.min}+` : count} contributions`;
        }
        default: {
            return `No contributions`;
        }
    }
};

export const getTooltipDescription = (date: Dayjs): string => {
    return date.format(TOOLTIP_DATE_FORMAT);
}
