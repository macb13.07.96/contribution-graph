import {GRAPH_DAY_COLORS} from "../constants/styled";
import {GRAPH_ENTRIES} from "../constants/graph";

export const getDayColor = (count: number): string => {
    const {SMALL, MEDIUM, HIGH, VERY_HIGH} = GRAPH_ENTRIES;

    switch (true) {
        case count >= SMALL.min && count <= SMALL.max: {
            return GRAPH_DAY_COLORS.SMALL;
        }
        case count >= MEDIUM.min && count <= MEDIUM.max: {
            return GRAPH_DAY_COLORS.MEDIUM;
        }
        case count >= HIGH.min && count <= HIGH.max: {
            return GRAPH_DAY_COLORS.HIGH;
        }
        case count >= VERY_HIGH.min: {
            return GRAPH_DAY_COLORS.VERY_HIGH;
        }
        default: {
            return GRAPH_DAY_COLORS.BASE;
        }
    }
}
